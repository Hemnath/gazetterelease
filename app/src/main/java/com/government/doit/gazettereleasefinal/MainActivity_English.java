package com.government.doit.gazettereleasefinal;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Timer;
import java.util.TimerTask;
public class MainActivity_English extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    Button btnaboutus1;
    Button btnwebsite;
    Button btngazzete;
    Button btncontact;

    public int currentimageindex=0;
    Timer timer;
    TimerTask task;
    ImageView slidingimage;

    private int[] IMAGE_IDS = {
            R.drawable.splash1, R.drawable.splash2, R.drawable.splash3,R.drawable.splash4

    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity__english);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Handler mHandler = new Handler();

        // Create runnable for posting
        final Runnable mUpdateResults = new Runnable() {
            public void run() {

                AnimateandSlideShow();

            }
        };

        int delay = 1000; // delay for 1 sec.

        int period = 3000; // repeat every 4 sec.

        Timer timer = new Timer();

        timer.scheduleAtFixedRate(new TimerTask() {

            public void run() {

                mHandler.post(mUpdateResults);

            }

        }, delay, period);

        btnaboutus1 = (Button) findViewById(R.id.button01);
        btnwebsite = (Button) findViewById(R.id.button02);
        btngazzete = (Button) findViewById(R.id.button3);
        btncontact = (Button) findViewById(R.id.button4);

        btnaboutus1.setOnClickListener(this);
        btnwebsite.setOnClickListener(this);
        btngazzete.setOnClickListener(this);
        btncontact.setOnClickListener(this);



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button01:

                Intent intent = new Intent(this, Aboutus_english.class);
                startActivity(intent);
                break;
            case R.id.button02:

                Intent i = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.doit.gov.np/"));
                startActivity(i);
                break;
            case R.id.button3:

                Intent intent3 = new Intent(this, PRM.class);
                startActivity(intent3);
                break;

            case R.id.button4:
                Intent intent4=new Intent(this,Contactus_English.class);
                startActivity(intent4);
                break;


        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }




    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_facebook) {

            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://www.facebook.com/DepartmentofIT/?fref=ts"));
            startActivity(i);
            // Handle the camera action
        } else if (id == R.id.nav_twitter) {

            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://twitter.com/departmentofi"));
            startActivity(i);

        } else if (id == R.id.nav_google) {


            Intent i = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("https://plus.google.com/u/0/114693812677759747745/posts"));
            startActivity(i);

        }
        else if (id == R.id.nav_location)

        {


            Intent i = new Intent(Intent.ACTION_VIEW,

                    Uri.parse("https://www.google.com/maps/place/IT+Park/@27.618676,85.5360193,13.5z/data=!4m2!3m1!1s0x0:0x3617b0efeae85c48?hl=en-US"));

            startActivity(i);

        }

        else if (id == R.id.nav_calender) {


            Intent intent3 = new Intent(this,com.government.doit.gazettereleasefinal.Calender.class);
            startActivity(intent3);

        }


        else if (id == R.id.nav_feedback)

        {

            Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

            emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

            emailIntent.setType("vnd.android.cursor.item/email");

            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"info@doit.gov.np"});

            emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Press Release");

            emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "");

            startActivity(Intent.createChooser(emailIntent, "Send mail using..."));

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    private void AnimateandSlideShow()

    {


        slidingimage = (ImageView)findViewById(R.id.imageTitle);

        slidingimage.setImageResource(IMAGE_IDS[currentimageindex % IMAGE_IDS.length]);

        currentimageindex++;

        Animation rotateimage = AnimationUtils.loadAnimation(this, R.anim.custom_anim);

    }

}
