package com.government.doit.gazettereleasefinal;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by dev on 8/26/15.
 */

public class
        SelectLanguage extends Activity implements View.OnClickListener {

    Button btnChangeNepali;
    Button btnChangeEnglish;
    int langauge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_language);

        //Find layout elements
        btnChangeNepali = (Button) findViewById(R.id.nepali);
        btnChangeEnglish = (Button) findViewById(R.id.english);

        btnChangeNepali.setOnClickListener(this);
        btnChangeEnglish.setOnClickListener(this);


    }



    public void onClick(View v) {

        switch (v.getId()) {

            //For Nepali
            case R.id.nepali:
                System.out.println("just changed to nepali");
                Intent intent = new Intent(this, com.government.doit.gazettereleasefinal.nepali.PRMNepali.class);
                startActivity(intent);
                break;

            //For English
            case R.id.english:
                System.out.println("just changed to english");
                Intent intent1 = new Intent(this, PRM.class);
                startActivity(intent1);
                break;

        }
    }


    @Override public void onBackPressed() {
        new AlertDialog.Builder(this).setIcon(android.R.drawable.ic_dialog_alert).setTitle("Closing Activity").setMessage("Are you sure you want to close this activity?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override public void onClick(DialogInterface dialog, int which) {
                finish();
            }

        }).setNegativeButton("No", null).show();
    }
}