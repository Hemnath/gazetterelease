package com.government.doit.gazettereleasefinal.helpers;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

@SuppressLint("SdCardPath")
public class FileDownloadHelper extends AsyncTask<String, String, String> {
	
	Context context;
	private ProgressDialog pDialog;
	String file_name;
	
	
	public FileDownloadHelper(Context context, String file_name) {
		super();
		this.context = context;
		this.file_name = file_name;
	}

	public static final int progress_bar_type =0;	
	
	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		super.onPostExecute(result);
		pDialog.dismiss();
//		Toast.makeText(context, "File is downloaded to: "+Environment.getExternalStorageDirectory() + "/Press_Release/"+file_name, Toast.LENGTH_LONG).show();
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
				context);

		// set title
		alertDialogBuilder.setTitle("Download Successful");
		// set dialog message
		alertDialogBuilder.setMessage("File is downloaded to: "+ Environment.getExternalStorageDirectory() + "/Press_Release/"+file_name)
				.setCancelable(false)
				.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
					}
				});
		// create alert dialog
		AlertDialog alertDialog = alertDialogBuilder.create();

		// show it
		alertDialog.show();
	}


	@Override
	protected void onPreExecute() {
		// TODO Auto-generated method stub
		super.onPreExecute();
		pDialog = new ProgressDialog(context);
		pDialog.setTitle("File Download");
        pDialog.setMessage("Downloading. Please wait...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(true);
        pDialog.show();
	}


	@Override
	protected void onProgressUpdate(String... values) {
		// TODO Auto-generated method stub
		super.onProgressUpdate(values);
		pDialog.setProgress(Integer.parseInt(values[0]));
	}


	@Override
	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		int count;
        try {
            URL url = new URL(params[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            // this will be useful so that you can show a tipical 0-100% progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
            File dir = new File("/sdcard/Press_Release");
            OutputStream output=null;
            File f = new File(Environment.getExternalStorageDirectory() + "/Press_Release");
            if(!f.isDirectory()) {
            	dir.mkdir();
            }
            output = new FileOutputStream(Environment.getExternalStorageDirectory() + "/Press_Release/"+file_name);
			
            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress(""+(int)((total*100)/lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
        	e.printStackTrace();
//            Log.e("Error: ", e.getMessage());
        }
		return null;
	}

	
}
