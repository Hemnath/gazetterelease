package com.government.doit.gazettereleasefinal.models;

public class MinsAndDepts {
	
	int minId;
	String ministry;
	int deptId;
	String dept;
	
	public int getMinId() {
		return minId;
	}
	public void setMinId(int minId) {
		this.minId = minId;
	}
	public String getMinistry() {
		return ministry;
	}
	public void setMinistry(String ministry) {
		this.ministry = ministry;
	}
	public int getDeptId() {
		return deptId;
	}
	public void setDeptId(int deptId) {
		this.deptId = deptId;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}

}
