package com.government.doit.gazettereleasefinal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class Calender extends AppCompatActivity {
    private static TextView button_sbn;
    String Url = "https://calendar.google.com/calendar/embed?src=departmentofi%40gmail.com&ctz=Asia/Katmandu%22%20style=%22border:%200%22%20width=%22800%22%20height=%22600%22%20frameborder=%220%22%20scrolling=%22no%22";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calender);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        android.webkit.WebView webView = (android.webkit.WebView) findViewById(R.id.webView1);
        webView.getSettings().setJavaScriptEnabled(true);
        
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(Url);
    }

}
