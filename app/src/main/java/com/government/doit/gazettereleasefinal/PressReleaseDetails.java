package com.government.doit.gazettereleasefinal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;

import com.government.doit.gazettereleasefinal.helpers.ConnectionMngr;
import com.government.doit.gazettereleasefinal.helpers.FileDownloadHelper;

public class PressReleaseDetails extends Activity {


	String pdfLink, id, date, title, ministry;
	WebView webView;

	ConnectionMngr cm;
	TextView tvDate, tvTitle, tvMinistry;
	Button btnDwnld, btnSendMail;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		Intent intent = getIntent();

//		ActionBar actionBar = getActionBar();
//		actionBar.setHomeButtonEnabled(true);
		
		cm = new ConnectionMngr(PressReleaseDetails.this);
		
		id = intent.getStringExtra("id");
		pdfLink = intent.getStringExtra("pdfLink");
		date = intent.getStringExtra("date");
		title = intent.getStringExtra("title");
		ministry = intent.getStringExtra("ministry");

		System.out.println("Link : "+pdfLink);
		
		setContentView(R.layout.press_release_detail);

		tvTitle = (TextView) findViewById(R.id.tvTitle);
		tvDate = (TextView) findViewById(R.id.tvDate);
		tvMinistry = (TextView) findViewById(R.id.tvMinistry);
		btnDwnld = (Button) findViewById(R.id.btnDwnld);
		btnSendMail = (Button) findViewById(R.id.btnSendMail);

		tvTitle.setText(title);
		tvDate.setText(date);
		tvMinistry.setText(ministry);

		if(cm.hasConnection()){
			webView = (WebView) findViewById(R.id.wvPressRelease);
			
			webView.getSettings().setLoadWithOverviewMode(true);
			webView.getSettings().setUseWideViewPort(true);
			webView.getSettings().setLoadsImagesAutomatically(true);
			webView.getSettings().setJavaScriptEnabled(true);
			webView.getSettings().setBuiltInZoomControls(true);
			webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
			webView.loadUrl("https://docs.google.com/gview?embedded=true&url="
					+ pdfLink);
			webView.setWebViewClient(new HelloWebViewClient()); 
			
			
			btnDwnld.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					
					
					StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
					StrictMode.setThreadPolicy(policy);
					if(pdfLink.equals("null")){
						Toast.makeText(getApplicationContext(), "404 Error: ओहो!! फाइल त भेटिएन नि", Toast.LENGTH_LONG).show();
					}else{
					
						try {
						    URL url = new URL(pdfLink);
						    String file_name;
						    String arr[] = pdfLink.split("/");
						    file_name=arr[arr.length-1];
						    HttpURLConnection huc = (HttpURLConnection) url.openConnection();
						    huc.setRequestMethod("GET"); 
						    huc.connect() ; 
						    int responseCode = huc.getResponseCode();
						    if(responseCode == 200){
						    	FileDownloadHelper fdh = new FileDownloadHelper(PressReleaseDetails.this, file_name);
						    	fdh.execute(pdfLink);
						    }else{
						    	Toast.makeText(getApplicationContext(), "404 Error: ओहो!! फाइल त भेटिएन नि", Toast.LENGTH_LONG).show();
						    }
						    // Handle response code here...
						} catch (UnknownHostException uhe) {
							uhe.printStackTrace();
							Log.d("Error:", "error 1");
						    // Handle exceptions as necessary
						} catch (FileNotFoundException fnfe) {
						    // Handle exceptions as necessary
							fnfe.printStackTrace();
							Log.d("Error:", "error 1");
						} catch (Exception e) {
							e.printStackTrace();
							Log.d("Error:", "error 1");
						    // Handle exceptions as necessary
						}
					}
					
				}
			});
			
			btnSendMail.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
	
					Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
					emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					emailIntent.setType("vnd.android.cursor.item/email");
	//				emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[] {"abc@xyz.com"});
					emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Press Release");
					emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Please view this press release: "+ pdfLink);
					startActivity(Intent.createChooser(emailIntent, "Send mail using..."));
				}
			});

		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
     	   view.loadUrl(url);
            return true;
        }
  }

}
