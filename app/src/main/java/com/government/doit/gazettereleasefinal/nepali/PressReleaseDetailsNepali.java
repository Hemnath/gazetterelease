package com.government.doit.gazettereleasefinal.nepali;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.net.URL;

public class PressReleaseDetailsNepali extends Activity {
    String pdfLink, id, date, title, ministry;
    WebView webView;
    com.government.doit.gazettereleasefinal.helpers.ConnectionMngr cm;
    TextView tvDate, tvTitle, tvMinistry;
    Button btnDwnld, btnSendMail;
    @android.annotation.SuppressLint("SetJavaScriptEnabled") @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent();
        cm = new com.government.doit.gazettereleasefinal.helpers.ConnectionMngr(PressReleaseDetailsNepali.this);
        id = intent.getStringExtra("id");
        pdfLink = intent.getStringExtra("pdfLink");
        date = intent.getStringExtra("date");
        title = intent.getStringExtra("title");
        ministry = intent.getStringExtra("ministry");
        System.out.println("Link : " + pdfLink);

        setContentView(com.government.doit.gazettereleasefinal.R.layout.press_release_detail_nepali);
        tvTitle = (TextView) findViewById(com.government.doit.gazettereleasefinal.R.id.tvTitle);
        tvDate = (TextView) findViewById(com.government.doit.gazettereleasefinal.R.id.tvDate);
        tvMinistry = (TextView) findViewById(com.government.doit.gazettereleasefinal.R.id.tvMinistry);
        btnDwnld = (Button) findViewById(com.government.doit.gazettereleasefinal.R.id.btnDwnld);
        btnSendMail = (Button) findViewById(com.government.doit.gazettereleasefinal.R.id.btnSendMail);

        tvTitle.setText(title);
        tvDate.setText(date);
        tvMinistry.setText(ministry);

        if (cm.hasConnection()) {
            webView = (WebView) findViewById(com.government.doit.gazettereleasefinal.R.id.wvPressRelease);
            webView.getSettings().setLoadWithOverviewMode(true);
            webView.getSettings().setUseWideViewPort(true);
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.getSettings().setBuiltInZoomControls(true);
            webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + pdfLink);
            webView.setWebViewClient(new HelloWebViewClient());

            btnDwnld.setOnClickListener(new android.view.View.OnClickListener() {

                @Override
                public void onClick(android.view.View arg0) {
                    ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder().permitAll().build();
                    StrictMode.setThreadPolicy(policy);
                    if (pdfLink.equals("null")) {
                        Toast.makeText(getApplicationContext(), "404 Error: ओहो!! फाइल त भेटिएन नि", Toast.LENGTH_LONG).show();
                    } else {
                        try {
                            URL url = new java.net.URL(pdfLink);
                            String file_name;
                            String arr[] = pdfLink.split("/");
                            file_name = arr[arr.length - 1];
                            java.net.HttpURLConnection huc = (java.net.HttpURLConnection) url.openConnection();
                            huc.setRequestMethod("GET");
                            huc.connect();
                            int responseCode = huc.getResponseCode();
                            if (responseCode == 200) {
                                com.government.doit.gazettereleasefinal.helpers.FileDownloadHelperNepali fdh = new com.government.doit.gazettereleasefinal.helpers.FileDownloadHelperNepali(PressReleaseDetailsNepali.this, file_name);
                                fdh.execute(pdfLink);
                            } else {
                                Toast.makeText(getApplicationContext(), "404 Error: ओहो!! फाइल त भेटिएन नि", Toast.LENGTH_LONG).show();
                            }
                            // Handle response code here...
                        } catch (java.net.UnknownHostException uhe) {
                            uhe.printStackTrace();
                            android.util.Log.d("Error:", "error 1");
                            // Handle exceptions as necessary
                        } catch (java.io.FileNotFoundException fnfe) {
                            // Handle exceptions as necessary
                            fnfe.printStackTrace();
                            android.util.Log.d("Error:", "error 1");
                        } catch (Exception e) {
                            e.printStackTrace();
                            android.util.Log.d("Error:", "error 1");
                            // Handle exceptions as necessary
                        }
                    }

                }
            });

            btnSendMail.setOnClickListener(new android.view.View.OnClickListener() {

                @Override
                public void onClick(android.view.View v) {
                    Intent emailIntent = new Intent(Intent.ACTION_SEND);
                    emailIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    emailIntent.setType("vnd.android.cursor.item/email");
                    emailIntent.putExtra(Intent.EXTRA_SUBJECT, "प्रेस विज्ञप्ति");
                    emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, "कृपया यो प्रेस विज्ञप्ति हेर्नुहोला " + pdfLink);;
                    startActivity(Intent.createChooser(emailIntent, "प्रयोग गरेर ईमेल पठाउनुहोस..."));
                }
            });
        }
    }

    @Override
    public boolean onOptionsItemSelected(android.view.MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class HelloWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }

}
