package com.government.doit.gazettereleasefinal;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Contactus_Nepali extends AppCompatActivity {
    private static TextView button_sbn;
    String mapUrl = "https://www.google.com/maps/place/IT+Park/@27.6544249,85.4553203,10.5z/data=!4m2!3m1!1s0x0:0x3617b0efeae85c48?hl=en-US";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus__nepali);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        onClickButtonListener();


        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Contactus_Nepali.this, MainActivity.class));


            }
        });

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        android.webkit.WebView webView = (android.webkit.WebView) findViewById(com.government.doit.gazettereleasefinal.R.id.wvMap);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(mapUrl);


    }
    public void onClickButtonListener() {
        button_sbn = (TextView) findViewById(R.id.textView6);
        button_sbn.setOnClickListener(new android.view.View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              Intent callIntent = new android.content.Intent(Intent.ACTION_CALL);
                                              callIntent.setData(android.net.Uri.parse("tel:014491439"));
                                              if (ActivityCompat.checkSelfPermission(Contactus_Nepali.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                  // TODO: Consider calling
                                                  //    ActivityCompat#requestPermissions
                                                  // here to request the missing permissions, and then overriding
                                                  //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                                  //                                          int[] grantResults)
                                                  // to handle the case where the user grants the permission. See the documentation
                                                  // for ActivityCompat#requestPermissions for more details.
                                                  return;
                                              }
                                              startActivity(callIntent);
                                          }
                                      }

        );

        button_sbn = (TextView) findViewById(R.id.textView7);
        button_sbn.setOnClickListener(new android.view.View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              Intent callIntent = new android.content.Intent(Intent.ACTION_CALL);
                                              callIntent.setData(android.net.Uri.parse("tel:014491598"));
                                              if (ActivityCompat.checkSelfPermission(Contactus_Nepali.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                  // TODO: Consider calling
                                                  //    ActivityCompat#requestPermissions
                                                  // here to request the missing permissions, and then overriding
                                                  //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                                  //                                          int[] grantResults)
                                                  // to handle the case where the user grants the permission. See the documentation
                                                  // for ActivityCompat#requestPermissions for more details.
                                                  return;
                                              }
                                              startActivity(callIntent);
                                          }
                                      }

        );
    }

}
