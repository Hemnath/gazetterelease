package com.government.doit.gazettereleasefinal;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.content.Intent;

public class Select_Language1 extends AppCompatActivity implements View.OnClickListener {
    Button btnChangeNepali;
    Button btnChangeEnglish;
    int langauge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select__language1);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnChangeNepali = (Button) findViewById(R.id.nepali);
        btnChangeEnglish = (Button) findViewById(R.id.english);

        btnChangeNepali.setOnClickListener(this);
        btnChangeEnglish.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            //For Nepali
            case R.id.nepali:
                System.out.println("just changed to nepali");
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;

            //For English
            case R.id.english:
                System.out.println("just changed to english");
                Intent intent1 = new Intent(this, MainActivity_English.class);
                startActivity(intent1);
                break;

        }
    }
}
