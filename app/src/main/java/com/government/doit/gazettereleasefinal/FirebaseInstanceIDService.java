package com.government.doit.gazettereleasefinal;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.IOException;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;


public class FirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {

        String token = FirebaseInstanceId.getInstance().getToken();

        registerToken(token);
    }
    private void registerToken(String token) {
        Log.d("onTokenRefresh","Refreshed token: " + token);
        System.out.print("Refreshed token: " + token);
        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("regId",token)
                .build();
        System.out.println( "Token ID : " + body);
        Request request = new Request.Builder()
                .url("http://27.111.21.62/doit/index.php/api/app/device_id")
                .post(body)
                .build();

        try {
            client.newCall(request).execute();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
