package com.government.doit.gazettereleasefinal.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.government.doit.gazettereleasefinal.R;

public class ConnectionMngr {

	private static Context _cntx;

	public ConnectionMngr(Context cntx){
		_cntx = cntx;
	}

	public boolean hasConnection(){
		ConnectivityManager cm = (ConnectivityManager) _cntx.getSystemService(Activity.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if(networkInfo != null && networkInfo.isConnected())
			return true;
		else{
			AlertDialog.Builder dialogNetworkState = new AlertDialog.Builder(
					_cntx);

			dialogNetworkState.setTitle("Internet connection error !")
					.setMessage("Please connect your device to the internet")
//        				.setIcon(android.R.drawable.ic_delete)
					.setIcon(R.drawable.fail)
					.setPositiveButton("OK", new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {

							Activity activity = (Activity) _cntx;
							activity.finish();
							dialog.cancel();

						}
					});

			AlertDialog dialog = dialogNetworkState.create();
			dialog.show();
			return false;
		}
	}

}
