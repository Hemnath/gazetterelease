package com.government.doit.gazettereleasefinal.models;

public class PressRelease {

    String title;
    String date;
    String pdfLink;
    String ministry;
    int id;

    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public String getPdfLink() {
        return pdfLink;
    }
    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }
    public String getMinistry() {
        return ministry;
    }
    public void setMinistry(String ministry) {
        this.ministry = ministry;
    }

}
