package com.government.doit.gazettereleasefinal;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.government.doit.gazettereleasefinal.fragments.PressReleaseSearchFragment;

public class PressReleaseSearch extends Activity {

	String fromDate, toDate, freeText, min_dept;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_prm);

		//ActionBar actionBar = getActionBar();
		//actionBar.setHomeButtonEnabled(true);
		
		Intent intent = getIntent();
		fromDate = intent.getExtras().get("fromDate").toString();
		toDate = intent.getExtras().get("toDate").toString();
		freeText = intent.getExtras().get("freeText").toString();
		min_dept = intent.getExtras().get("min_dept").toString();

		Bundle args = new Bundle();
		//
		args.putString("fromDate", fromDate);
		args.putString("toDate", toDate);
		args.putString("freeText", freeText);
		args.putString("min_dept", min_dept);

		if (savedInstanceState == null) {
			Fragment myFragment = new PressReleaseSearchFragment();
			myFragment.setArguments(args);
			getFragmentManager().beginTransaction()
					.add(R.id.container, myFragment).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}
}
