package com.government.doit.gazettereleasefinal;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

public class Contactus_English extends AppCompatActivity  {

    private static TextView button_sbn;
    String mapUrl = "https://www.google.com/maps/place/27%C2%B041'32.3%22N+85%C2%B019'53.3%22E/@27.6923194,85.3308105,309m/data=!3m2!1e3!4b1!4m5!3m4!1s0x0:0x0!8m2!3d27.692318!4d85.331465?hl=en-US";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactus__english);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        onClickButtonListener();
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Contactus_English.this, MainActivity.class));
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        android.webkit.WebView webView = (android.webkit.WebView) findViewById(com.government.doit.gazettereleasefinal.R.id.wvMap);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.setWebViewClient(new android.webkit.WebViewClient());
        webView.loadUrl(mapUrl);
    }
    public void onClickButtonListener() {
        button_sbn = (TextView) findViewById(R.id.textView6);
        button_sbn.setOnClickListener(new android.view.View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              Intent callIntent = new android.content.Intent(Intent.ACTION_CALL);
                                              callIntent.setData(android.net.Uri.parse("tel:014491439"));
                                              if (ActivityCompat.checkSelfPermission(Contactus_English.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                  // TODO: Consider calling

                                                  return;
                                              }
                                              startActivity(callIntent);
                                          }
                                      }

        );

        button_sbn = (TextView) findViewById(R.id.textView7);
        button_sbn.setOnClickListener(new android.view.View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {
                                              Intent callIntent = new android.content.Intent(Intent.ACTION_CALL);
                                              callIntent.setData(android.net.Uri.parse("tel:014491598"));
                                              if (ActivityCompat.checkSelfPermission(Contactus_English.this, android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                                  // TODO: Consider calling

                                                  return;
                                              }
                                              startActivity(callIntent);
                                          }
                                      }

        );
    }




}
