package com.government.doit.gazettereleasefinal.helpers;

public class ConnectionMngrNepali {

	private static android.content.Context _cntx;

	public ConnectionMngrNepali(android.content.Context cntx){
		_cntx = cntx;
	}

	public boolean hasConnection(){
		android.net.ConnectivityManager cm = (android.net.ConnectivityManager) _cntx.getSystemService(android.app.Activity.CONNECTIVITY_SERVICE);
		android.net.NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if(networkInfo != null && networkInfo.isConnected())
			return true;
		else{
			android.app.AlertDialog.Builder dialogNetworkState = new android.app.AlertDialog.Builder(
					_cntx);

        	dialogNetworkState.setTitle("इन्टरनेट जडानमा त्रुटी !")
        				.setMessage("कृपया यो उपकरणलाई इन्टरनेट संग जोडनुहोस")
//        				.setIcon(android.R.drawable.ic_delete)
        				.setIcon(com.government.doit.gazettereleasefinal.R.drawable.fail)
        				.setPositiveButton("OK", new android.content.DialogInterface.OnClickListener() {

							@Override
							public void onClick(android.content.DialogInterface dialog, int which) {

								android.app.Activity activity = (android.app.Activity) _cntx;
								activity.finish();
								dialog.cancel();

							}
						});

        	android.app.AlertDialog dialog = dialogNetworkState.create();
        	dialog.show();
        	return false;
		}
	}
	
}
