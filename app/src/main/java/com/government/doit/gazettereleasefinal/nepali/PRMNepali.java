package com.government.doit.gazettereleasefinal.nepali;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;

public class PRMNepali extends Activity {

    com.government.doit.gazettereleasefinal.helpers.ConnectionMngrNepali cm;
    android.widget.ProgressBar ProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.government.doit.gazettereleasefinal.R.layout.activity_prm);
        ProgressBar = (android.widget.ProgressBar) findViewById(com.government.doit.gazettereleasefinal.R.id.progressBarPrm);
        cm = new com.government.doit.gazettereleasefinal.helpers.ConnectionMngrNepali(PRMNepali.this);
        if (cm.hasConnection()) {
            if (savedInstanceState == null) {
                getFragmentManager().beginTransaction().add(com.government.doit.gazettereleasefinal.R.id.container, new com.government.doit.gazettereleasefinal.fragments.nepali.PressReleaseFragmentNepali()).commit();
            }
        }
    }

    public void showProgressBar() {
        ProgressBar.setVisibility(View.VISIBLE);
        Runnable mRunnable;
        Handler mHandler = new android.os.Handler();
        mRunnable = new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                ProgressBar.setVisibility(View.INVISIBLE); //If you want just hide the View. But it will retain space occupied by the View.
                // ProgressBarRecipe.setVisibility(View.GONE); //This will remove the View. and free s the space occupied by the View
            }
        };
        mHandler.postDelayed(mRunnable, 3 * 1000);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}