package com.government.doit.gazettereleasefinal;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.gcm.GCMRegistrar;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

public class LandingScreen extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.landing_screen);
		FirebaseMessaging.getInstance().subscribeToTopic("test");
		FirebaseInstanceId.getInstance().getToken();
		Thread timer = new Thread() {
            public void run(){
                try {
                    sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally{
                	Intent i = new Intent(LandingScreen.this,SelectLanguage.class);
                	startActivity(i);
                	finish();
                }
            }
		};
		timer.start();
	}
}
