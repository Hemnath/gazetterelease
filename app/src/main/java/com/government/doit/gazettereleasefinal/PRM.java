package com.government.doit.gazettereleasefinal;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;

import com.government.doit.gazettereleasefinal.fragments.PressReleaseFragment;
import com.government.doit.gazettereleasefinal.helpers.ConnectionMngr;

public class PRM extends Activity {
	
	ConnectionMngr cm;
	android.widget.ProgressBar ProgressBar;

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prm);
        ProgressBar=(android.widget.ProgressBar)findViewById(R.id.progressBarPrm);
        
        cm = new ConnectionMngr(PRM.this);
//
//        ActionBar actionBar = getActionBar();
//        actionBar.setHomeButtonEnabled(false);
//
        
        
        if(cm.hasConnection()){
	        if (savedInstanceState == null) {
	            getFragmentManager().beginTransaction()
	                    .add(R.id.container, new PressReleaseFragment())
	                    .commit();
	        }
        }   
      //  showProgressBar();
    }
	
	public void showProgressBar() {
        ProgressBar.setVisibility(View.VISIBLE);

        Runnable mRunnable;
        Handler mHandler = new Handler();

        mRunnable = new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                ProgressBar.setVisibility(View.INVISIBLE); //If you want just hide the View. But it will retain space occupied by the View.
                // ProgressBarRecipe.setVisibility(View.GONE); //This will remove the View. and free s the space occupied by the View
            }
        };


        mHandler.postDelayed(mRunnable, 3 * 1000);
    }
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
	    switch (item.getItemId()) {
	        case android.R.id.home:
	        	finish();
	            return true;
	        default:
	            return super.onOptionsItemSelected(item);
	    }
	}

}