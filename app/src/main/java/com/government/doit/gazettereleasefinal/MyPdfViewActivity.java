package com.government.doit.gazettereleasefinal;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.MenuItem;
import android.webkit.WebView;

public class MyPdfViewActivity extends Activity {

    WebView mWebView;
    String pdfLink = "https://www.adobe.com/enterprise/accessibility/pdfs/acro6_pg_ue.pdf";

    @SuppressLint("SetJavaScriptEnabled") @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.press_release_detail);
        mWebView = (WebView) findViewById(R.id.wvPressRelease);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + pdfLink);
        	 //   ActionBar ab = getActionBar();
        	  //  ab.setHomeButtonEnabled(true);
    }

    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        mWebView.loadUrl("https://docs.google.com/gview?embedded=true&url=" + pdfLink);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        boolean flag = false;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                flag = true;
        }

        return flag;
    }


}