package com.government.doit.gazettereleasefinal.fragments;

import android.app.ActionBar.LayoutParams;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v4.widget.SwipeRefreshLayout.OnRefreshListener;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.government.doit.gazettereleasefinal.PressReleaseDetails;
import com.government.doit.gazettereleasefinal.PressReleaseSearch;
import com.government.doit.gazettereleasefinal.R;
import com.government.doit.gazettereleasefinal.adapters.PressReleaseAdapter;
import com.government.doit.gazettereleasefinal.helpers.URLHelper;
import com.government.doit.gazettereleasefinal.models.MinsAndDepts;
import com.government.doit.gazettereleasefinal.models.PressRelease;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * A placeholder fragment containing a simple view.
 */
public class PressReleaseFragment extends Fragment {

    int urlCall = 0;
    View view;

    ArrayList<PressRelease> alPressRelease;
    ArrayList<MinsAndDepts> alMinsAndDepts;
    String[] strMinsAndDepts;

    String min_dept = "", fromDay = "", fromMonth = "", fromYear = "", toDay = "", toMonth = "", toYear = "";
    String fromDate = "";
    String toDate = "", freeText = "";

    Spinner spnMinistry, spnFromDays, spnFromMonths, spnFromYears, spnToDays, spnToMonths, spnToYears;

    EditText etFreeText;
    ArrayList<HashMap<String, String>> searchDetails = new ArrayList<HashMap<String, String>>();
    ListView listView;
    ProgressBar progress;
    SwipeRefreshLayout sl;
    JSONFromUrl jsonPressRelease;

    public PressReleaseFragment() {
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_prm, container, false);

        this.view = view;
        listView = (ListView) view.findViewById(R.id.lvLatestRelease);
        sl = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);
        progress = (ProgressBar) view.findViewById(R.id.prog);
        listView.setOnScrollListener(new OnScrollListener() {

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                // TODO Auto-generated method stub
                Boolean hooo = listIsAtTop();

                System.out.println("------------hooo----------------" + hooo);
                //	System.out.println("------------firstVisibleItem----------------"+firstVisibleItem);


            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });

        alPressRelease = new ArrayList<PressRelease>();
        alMinsAndDepts = new ArrayList<MinsAndDepts>();

        jsonPressRelease = new JSONFromUrl();
        jsonPressRelease.execute(URLHelper.get_preleases);

        JSONFromUrl jsonMinistries = new JSONFromUrl();
        jsonMinistries.execute(URLHelper.get_mins_depts);

        final ArrayAdapter<CharSequence> monthsAdapter = ArrayAdapter.createFromResource(view.getContext(), R.array.english_months, android.R.layout.simple_spinner_item);
        monthsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> daysAdapter = ArrayAdapter.createFromResource(view.getContext(), R.array.english_days, android.R.layout.simple_spinner_item);
        daysAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        final ArrayAdapter<CharSequence> yearsAdapter = ArrayAdapter.createFromResource(view.getContext(), R.array.english_years, android.R.layout.simple_spinner_item);
        yearsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ImageButton btnSrch = (ImageButton) view.findViewById(R.id.btnSrch);
        btnSrch.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                fromDate = "";
                toDate = "";
                fromMonth = "";
                fromDay = "";
                fromYear = "";
                toMonth = "";
                toDay = "";
                toYear = "";

                final PopupWindow pw;
                pw = new PopupWindow(inflater.inflate(R.layout.popup_search, null, false), LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);

                pw.getContentView().setFocusableInTouchMode(true);
                pw.getContentView().setOnKeyListener(new OnKeyListener() {

                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (keyCode == KeyEvent.KEYCODE_BACK) {
                            pw.dismiss();
                        }
                        return false;
                    }
                });

                ArrayAdapter<String> adptrMinsAndDepts = null;
                try {

                    adptrMinsAndDepts = new ArrayAdapter<String>(view.getContext(), android.R.layout.simple_spinner_item, strMinsAndDepts);
                } catch (Exception e) {
                }
                if (null != adptrMinsAndDepts) {
                    adptrMinsAndDepts.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                    spnMinistry = (Spinner) pw.getContentView().findViewById(R.id.spnMinistry);
                    spnFromDays = (Spinner) pw.getContentView().findViewById(R.id.spnFromDay);
                    spnFromMonths = (Spinner) pw.getContentView().findViewById(R.id.spnFromMonth);
                    spnFromYears = (Spinner) pw.getContentView().findViewById(R.id.spnFromYear);
                    spnToDays = (Spinner) pw.getContentView().findViewById(R.id.spnToDay);
                    spnToMonths = (Spinner) pw.getContentView().findViewById(R.id.spnToMonth);
                    spnToYears = (Spinner) pw.getContentView().findViewById(R.id.spnToYear);
                    etFreeText = (EditText) pw.getContentView().findViewById(R.id.etSearch);

                    spnMinistry.setAdapter(adptrMinsAndDepts);
                    spnFromDays.setAdapter(daysAdapter);
                    spnFromMonths.setAdapter(monthsAdapter);
                    spnFromYears.setAdapter(yearsAdapter);
                    spnToDays.setAdapter(daysAdapter);
                    spnToMonths.setAdapter(monthsAdapter);
                    spnToYears.setAdapter(yearsAdapter);

                    spnMinistry.setOnItemSelectedListener(spnItemSelectedListener);
                    spnFromDays.setOnItemSelectedListener(spnItemSelectedListener);
                    spnFromMonths.setOnItemSelectedListener(spnItemSelectedListener);
                    spnFromYears.setOnItemSelectedListener(spnItemSelectedListener);
                    spnToDays.setOnItemSelectedListener(spnItemSelectedListener);
                    spnToMonths.setOnItemSelectedListener(spnItemSelectedListener);
                    spnToYears.setOnItemSelectedListener(spnItemSelectedListener);

//				spnMinistry.getSelectedItem();
                    getMinistryToSearch(spnMinistry.getSelectedItemPosition());
                    Button btnPopUpSearch = (Button) pw.getContentView().findViewById(R.id.btnPopUpSrch);
                    Button btnCancel = (Button) pw.getContentView().findViewById(R.id.btnCancel);
                    pw.showAtLocation(view.findViewById(R.id.fragment_prm), Gravity.CENTER, 0, 0);
                    btnCancel.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            pw.dismiss();
                        }

                    });

                    btnPopUpSearch.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            int frmMonthNum = getMonthNum(fromMonth);
                            int toMonthNum = getMonthNum(toMonth);
                            if (frmMonthNum != 0 && !fromYear.isEmpty() && !fromDay.isEmpty()) {
                                fromDate = fromYear + "-" + frmMonthNum + "-" + fromDay;
                            }

                            System.out.println("Fromdate: " + toYear + " to date: " + toDay);

                            if (toMonthNum != 0 && !toYear.isEmpty() && !toDay.isEmpty()) {
                                toDate = toYear + "-" + toMonthNum + "-" + toDay;
                            }
                            freeText = etFreeText.getText().toString();

                            System.out.println("Fromdate: " + fromDate + " to date: " + toDate);

                            Intent intent = new Intent(view.getContext(), PressReleaseSearch.class);

                            intent.putExtra("fromDate", fromDate);
                            intent.putExtra("toDate", toDate);
                            intent.putExtra("freeText", freeText);
                            intent.putExtra("min_dept", min_dept);

                            System.out.println("ministyr_department Is:" + min_dept);

                            startActivity(intent);

                            pw.dismiss();

                        }
                    });
                } else {

                    Toast.makeText(getActivity(), "Please wait......", Toast.LENGTH_LONG).show();
                }

            }


        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                int topRowVerticalPosition = (sl == null || sl.getChildCount() == 0) ? 0 : sl.getChildAt(0).getTop();
                // swipeContainer.setEnabled(firstVisibleItem == 0 && topRowVerticalPosition >= 0);

            }
        });

        sl.setOnRefreshListener(new OnRefreshListener() {

            @Override
            public void onRefresh() {
                showProgressBar();
                jsonPressRelease = new JSONFromUrl();
                jsonPressRelease.execute(URLHelper.get_preleases);
                // TODO Auto-generated method stub
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sl.setRefreshing(false);


                    }
                }, 6000);

            }
        });
        //   sl.setRotation(4.04f);
        sl.setColorScheme(android.R.color.white, android.R.color.white, android.R.color.white, android.R.color.white);

//	    sl.setColorSchemeResources(
//                R.xml.progress,
//                R.xml.progress,
//                R.xml.progress);

        return view;
    }

    public OnItemClickListener pressReleaseItemClickListener = new OnItemClickListener() {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int pos, long id) {
            TextView tvId = (TextView) view.findViewById(R.id.tvId);
            TextView tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            TextView tvDate = (TextView) view.findViewById(R.id.tvDate);
            TextView tvPdfLink = (TextView) view.findViewById(R.id.tvPdfLink);
            TextView tvMinistry = (TextView) view.findViewById(R.id.tvMinistry);

            Intent intent = new Intent(getActivity().getApplicationContext(), PressReleaseDetails.class);
            intent.putExtra("id", tvId.getText());
            intent.putExtra("title", tvTitle.getText());
            intent.putExtra("date", tvDate.getText());
            intent.putExtra("pdfLink", tvPdfLink.getText());
            intent.putExtra("ministry", tvMinistry.getText());
            startActivity(intent);

        }
    };

    private class JSONFromUrl extends AsyncTask<String, Void, String> {

        int urlCall = 0;
        String urlParameters = "";
        ProgressDialog pd;
        String res;

        @Override
        protected void onPreExecute() {
            // TODO Auto-generated method stub
            super.onPreExecute();
//			pd=new ProgressDialog(getActivity());
//			pd.getWindow().setGravity(Gravity.TOP);
//			
//			pd.show();
        }

        @Override
        protected String doInBackground(String... URLs) {


            StringBuffer response = new StringBuffer();

            try {

                URL obj = new URL(URLs[0]);
                System.out.println(URLs[0]);
                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                // add request header
                con.setRequestMethod("POST");


                if (URLs[0].equals(URLHelper.get_preleases)) {
                    urlCall = 1;
                } else urlCall = 2;

                // String urlParameters = "longitude=" + longitude
                // +"&latitude="+latitude;

                // Send post request
                con.setDoOutput(true);
                DataOutputStream wr = new DataOutputStream(con.getOutputStream());
                wr.writeBytes(urlParameters);
                wr.flush();
                wr.close();

                int responseCode = con.getResponseCode();
                if (responseCode == 200) {
                    BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    String inputLine;

                    while ((inputLine = in.readLine()) != null) {
                        response.append(inputLine);
                    }
                    in.close();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            return response.toString();
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            //pd.dismiss();

            Log.d("JSON", result);
            res = result;
            System.out.println("-----------PostExecuuuuuuuuuuuuuuuuuuuuuute99999999999999999999999999-------");

            switch (urlCall) {
                case 1: // for get_prelease
                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            getPressReleaseJson(res);
                        }
                    });

                    break;
                case 2: // for get_ministries and departments

                    getMinsAndDepts(result);

                    break;
            }

        }
    }

    @SuppressWarnings("deprecation") private void getPressReleaseJson(String json) {
        alPressRelease = new ArrayList<PressRelease>();
        Log.d("JSON", json);


        try {
            JSONArray jsonArray = new JSONArray(json);
            if (jsonArray.length() > 0) {
                for (int i = 0; i < jsonArray.length(); i++) {
                    PressRelease pressRelease = new PressRelease();

                    pressRelease.setId(jsonArray.getJSONObject(i).getInt("gazette_id"));
                    pressRelease.setTitle(jsonArray.getJSONObject(i).getString("gazette_title_en"));
                    pressRelease.setDate(jsonArray.getJSONObject(i).getString("gazette_ts"));
                    pressRelease.setPdfLink(jsonArray.getJSONObject(i).getString("file_url"));

                    String minName, depName;
                    minName = jsonArray.getJSONObject(i).getString("ministry_name_en");
                    depName = jsonArray.getJSONObject(i).getString("department_name_en");
                    if (depName.equals("null")) {
                        pressRelease.setMinistry(minName);
                    } else {
                        pressRelease.setMinistry(depName);
                    }

                    alPressRelease.add(pressRelease);


                    //ListView lv;
                    //lv = (ListView) view.findViewById(R.id.lvLatestRelease);

                    listView.setAdapter(new PressReleaseAdapter(view.getContext(), alPressRelease));

                    listView.setOnItemClickListener(pressReleaseItemClickListener);

                }
            } else {
                AlertDialog alertDialog1 = new AlertDialog.Builder(view.getContext()).create();
                alertDialog1.setTitle("Sorry");
                alertDialog1.setIcon(android.R.drawable.ic_dialog_info);
                alertDialog1.setMessage("No any press release found.");
                alertDialog1.setButton("OK", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        getActivity().finish();
                    }
                });
                alertDialog1.show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void getMinsAndDepts(String json) {
        JSONArray jsonArray;
        try {
            jsonArray = new JSONArray(json);
            strMinsAndDepts = new String[jsonArray.length() + 1];
            strMinsAndDepts[0] = "Select Ministry or Department";

            MinsAndDepts minsAndDepts = new MinsAndDepts();

            minsAndDepts.setMinId(0);
            minsAndDepts.setMinistry("");

            alMinsAndDepts.add(minsAndDepts);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObj = jsonArray.getJSONObject(i);
                minsAndDepts = new MinsAndDepts();

                minsAndDepts.setMinId(Integer.parseInt(jsonObj.getString("ministry_id")));
                minsAndDepts.setMinistry(jsonObj.getString("ministry_name_en"));
                int dept_id;
                if (jsonObj.getString("department_id").equals("null")) {
                    dept_id = 0;
                } else {
                    dept_id = Integer.parseInt(jsonObj.getString("department_id"));
                }
                minsAndDepts.setDeptId(dept_id);
                minsAndDepts.setDept(jsonObj.getString("department_name_en"));

                if (jsonObj.getString("department_name_en").equals("null")) {
                    strMinsAndDepts[i + 1] = jsonObj.getString("ministry_name_en");
                } else {
                    strMinsAndDepts[i + 1] = jsonObj.getString("department_name_en");
                }

                alMinsAndDepts.add(minsAndDepts);

            }
//			Log.d("Ministry and epartments:", strMinsAndDepts.toString());
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private OnItemSelectedListener spnItemSelectedListener = new OnItemSelectedListener() {

        @Override
        public void onItemSelected(AdapterView<?> adapterView, View v, int pos, long id) {

            switch (adapterView.getId()) {

                case R.id.spnMinistry:
                    String minDept = getMinistryToSearch(pos);
                    HashMap<String, String> hmMinistry = new HashMap<String, String>();
                    hmMinistry.put("ministry", minDept);
                    min_dept = minDept;
                    break;

                case R.id.spnFromDay:
                    fromDay = spnFromDays.getSelectedItem().toString();

                    break;

                case R.id.spnFromMonth:
                    fromMonth = spnFromMonths.getSelectedItem().toString();
                    break;

                case R.id.spnFromYear:
                    fromYear = spnFromYears.getSelectedItem().toString();
                    break;

                case R.id.spnToDay:
                    toDay = spnToDays.getSelectedItem().toString();
                    break;

                case R.id.spnToMonth:
                    toMonth = spnToMonths.getSelectedItem().toString();
                    break;

                case R.id.spnToYear:
                    toYear = spnToYears.getSelectedItem().toString();
                    break;

            }

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {
            // TODO Auto-generated method stub

        }
    };

    public String getMinistryToSearch(int pos) {
        MinsAndDepts minsAndDepts = new MinsAndDepts();
        minsAndDepts = alMinsAndDepts.get(pos);

        String minOrDept;
        String minId = Integer.toString(minsAndDepts.getMinId());
        String deptId;
        if (Integer.toString(minsAndDepts.getDeptId()).equals("null")) {
            deptId = "0";
        } else {
            deptId = Integer.toString(minsAndDepts.getDeptId());
        }
        minOrDept = minId + ":" + deptId;
        return minOrDept;

    }

    private int getMonthNum(String month) {

        int num = 0;

        if (month.equals("Baishakh")) num = 1;
        else if (month.equals("Jestha")) num = 2;
        else if (month.equals("Ashadh")) num = 3;
        else if (month.equals("Shrawan")) num = 4;
        else if (month.equals("Bhadra")) num = 5;
        else if (month.equals("Aswin")) num = 6;
        else if (month.equals("Kartik")) num = 7;
        else if (month.equals("Mangsir")) num = 8;
        else if (month.equals("Poush")) num = 9;
        else if (month.equals("Magh")) num = 10;
        else if (month.equals("Falgun")) num = 11;
        else if (month.equals("Chaitra")) num = 12;

        return num;
    }

    private boolean listIsAtTop() {

        if (listView.getChildCount() == 0) return true;
        return listView.getChildAt(0).getTop() == 0;
    }
    public void showProgressBar() {

        progress.setVisibility(View.VISIBLE);

        Runnable mRunnable;
        Handler mHandler = new Handler();

        mRunnable = new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                progress.setVisibility(View.GONE); //If you want just hide the View. But it will retain space occupied by the View.

                // ProgressBarRecipe.setVisibility(View.GONE); //This will remove the View. and free s the space occupied by the View
            }
        };


        mHandler.postDelayed(mRunnable, 6 * 1000);
    }

}
