package com.government.doit.gazettereleasefinal.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.StringTokenizer;

import com.government.doit.gazettereleasefinal.R;
import com.government.doit.gazettereleasefinal.models.PressRelease;

public class PressReleaseAdapter extends BaseAdapter {

	Context context;
	ArrayList<PressRelease> alPressRelease;
	PressRelease pressRelease;
	LayoutInflater mInflater;
	
		
	int id;
	String title, date, ministry, pdfLink;
	
	TextView tvId, tvTitle, tvDate, tvPdfLink, tvMore, tvMinistry;
	
	
	
	public PressReleaseAdapter(Context context,
							   ArrayList<PressRelease> alPressRelease) {
		super();
		this.context = context;
		this.alPressRelease = alPressRelease;
	}

	private class PressReleaseHolder{
		private TextView tvIdH, tvTitleH, tvDateH, tvPdfLinkH, tvMinistryH;
	}
//	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return alPressRelease.size();
	}

	@Override
	public Object getItem(int pos) {
		// TODO Auto-generated method stub
		return alPressRelease.get(pos);
	}

	@Override
	public long getItemId(int pos) {
		// TODO Auto-generated method stub
		return pos;
	}

	@Override
	public View getView(int pos, View view, ViewGroup parent) {
		
		PressReleaseHolder pressReleaseHolder = null;
		pressRelease = alPressRelease.get(pos);
		
		if(view == null){
			mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			view = mInflater.inflate(R.layout.press_release_list_view, parent, false);
			
			tvId		= (TextView) view.findViewById(R.id.tvId);
			tvTitle		= (TextView) view.findViewById(R.id.tvTitle);
			tvMinistry	= (TextView) view.findViewById(R.id.tvMinistry);
			tvDate		= (TextView) view.findViewById(R.id.tvDate);
			tvPdfLink	= (TextView) view.findViewById(R.id.tvPdfLink);
			tvMore		= (TextView) view.findViewById(R.id.tvMore);
			
			
			pressReleaseHolder = new PressReleaseHolder();
			pressReleaseHolder.tvIdH = tvId;
			pressReleaseHolder.tvTitleH = tvTitle;
			pressReleaseHolder.tvDateH = tvDate;
			pressReleaseHolder.tvMinistryH = tvMinistry;
			pressReleaseHolder.tvPdfLinkH = tvPdfLink;
			
			view.setTag(pressReleaseHolder);
		}
		
		if (pressReleaseHolder == null) {
			pressReleaseHolder = (PressReleaseHolder) view.getTag();
		}
		
		id		= pressRelease.getId();
		title	= pressRelease.getTitle();
		date	= pressRelease.getDate();
		pdfLink	= pressRelease.getPdfLink();
		ministry= pressRelease.getMinistry();
		
		pressReleaseHolder.tvIdH.setText(""+id);
		pressReleaseHolder.tvTitleH.setText(title);
		pressReleaseHolder.tvDateH.setText(date);
		pressReleaseHolder.tvPdfLinkH.setText(pdfLink);
		pressReleaseHolder.tvMinistryH.setText(ministry);
		
		return view;
	}

	public static String resizeString(String sentence, int nos) {

		String temp = "";
		StringTokenizer st = new StringTokenizer(sentence);
		if (st.countTokens() > nos) {
			for (int i = 0; i < nos && st.hasMoreTokens(); i++) {
				temp += st.nextToken() + " ";
			}
			temp += " ...";
		} else temp = sentence;
		return temp;
	}
	
}
