package com.government.doit.gazettereleasefinal.helpers;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

@android.annotation.SuppressLint("SdCardPath") public class FileDownloadHelperNepali extends AsyncTask<String, String, String> {
    Context context;
    private ProgressDialog pDialog;
    String file_name;

    public FileDownloadHelperNepali(Context context, String file_name) {
        super();
        this.context = context;
        this.file_name = file_name;
    }

    public static final int progress_bar_type = 0;

    @Override

    protected void onPostExecute(String result) {
        super.onPostExecute(result);
        pDialog.dismiss();
        Builder alertDialogBuilder = new AlertDialog.Builder(context);

        // set title
        alertDialogBuilder.setTitle("डाउनलोड सफल !");
        // set dialog message
        alertDialogBuilder.setMessage("फाइल डाउनलोड भएको ठांउ: " + Environment.getExternalStorageDirectory() + "/Press_Release/" + file_name).setCancelable(false).setPositiveButton("Ok", new android.content.DialogInterface.OnClickListener() {
            public void onClick(android.content.DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setTitle("फाइल डाउनलोड");
        pDialog.setMessage("डाउनलोड हुँदैछ! कृपया प्रतिक्षा गर्नुहोस...");
        pDialog.setIndeterminate(false);
        pDialog.setMax(100);
        pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        pDialog.setCancelable(true);
        pDialog.show();
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        pDialog.setProgress(Integer.parseInt(values[0]));
    }

    @Override
    protected String doInBackground(String... params) {
        int count;
        try {
            java.net.URL url = new URL(params[0]);
            URLConnection conection = url.openConnection();
            conection.connect();
            int lenghtOfFile = conection.getContentLength();
            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
            File dir = new java.io.File("/sdcard/Press_Release");
            OutputStream output = null;
            File f = new java.io.File(Environment.getExternalStorageDirectory() + "/Press_Release");
            if (!f.isDirectory()) {
                dir.mkdir();
            }
            output = new java.io.FileOutputStream(Environment.getExternalStorageDirectory() + "/Press_Release/" + file_name);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                // writing data to file
                output.write(data, 0, count);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
