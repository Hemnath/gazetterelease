package com.government.doit.gazettereleasefinal.nepali;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;

public class PressReleaseSearchNepali extends Activity {

	String fromDate, toDate, freeText, min_dept;

	@Override
	protected void onCreate(android.os.Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(com.government.doit.gazettereleasefinal.R.layout.activity_prm);

		Intent intent = getIntent();
		fromDate = intent.getExtras().get("fromDate").toString();
		toDate = intent.getExtras().get("toDate").toString();
		freeText = intent.getExtras().get("freeText").toString();
		min_dept = intent.getExtras().get("min_dept").toString();

		Bundle args = new android.os.Bundle();
		//
		args.putString("fromDate", fromDate);
		args.putString("toDate", toDate);
		args.putString("freeText", freeText);
		args.putString("min_dept", min_dept);

		if (savedInstanceState == null) {
			Fragment myFragment = new com.government.doit.gazettereleasefinal.fragments.nepali.PressReleaseSearchFragmentNepail();
			myFragment.setArguments(args);
			getFragmentManager().beginTransaction()
					.add(com.government.doit.gazettereleasefinal.R.id.container, myFragment).commit();
		}
	}

	@Override
	public boolean onOptionsItemSelected(android.view.MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				finish();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
}
